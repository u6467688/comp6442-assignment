package com.bullshit.flappy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    MyView mv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mv=findViewById(R.id.myview);
        mv.addObserver(this);


    }
    public void update(String str){
        if(str=="end"){
            Intent intent=new Intent(this, StartActivity.class);
            System.out.println(intent);
            startActivity(intent);
        }
    }
}