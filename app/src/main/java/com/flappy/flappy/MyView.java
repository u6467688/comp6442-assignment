package com.flappy.flappy;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.bullshit.flappy.R;

import java.util.Random;


public class MyView extends View implements Runnable {
    float xt = 0.0f;
    float yt = 0.0f;
    Handler timer;
    int width,height;
    String[] strings;
    float offset=0;
    int rollTimes=0;
    Resources mResources;
    Canvas canvas;
    MainActivity ma;
    public void addObserver(MainActivity ma){
        this.ma=ma;
    }
    public MyView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        preload();

        timer = new Handler();
        timer.postDelayed(this,10);


    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.canvas=canvas;
        draw();

//        Paint p = new Paint();
//        p.setColor(Color.BLACK);
//
//        p.setTextAlign(Paint.Align.CENTER);
//        p.setStrokeWidth(30.0f);
//        p.setTextSize(h/6);
//        p.setStyle(Paint.Style.STROKE);
//
//        canvas.drawRect(0,h/3,w,2*h/3,p);
//        p.setStyle(Paint.Style.FILL);
//        p.setStrokeWidth(0);
//        for(int i=0;i<strings.length;i++){
//            canvas.drawText(strings[i],w/2,(h/3)*((i+offset)%strings.length)-h/8,p);
//        }

//        System.out.println(Integer.toString(w));
    }


    @Override
    protected void onMeasure(int wspec, int hspec) {
//        System.out.println(MeasureSpec.toString(wspec));
        Log.d("MyView", "w:" + MeasureSpec.toString(wspec) +
            " h:" + MeasureSpec.toString(hspec));
        setMeasuredDimension(MeasureSpec.getSize(wspec), MeasureSpec.getSize(hspec));
        width= MeasureSpec.getSize(wspec);
        height= MeasureSpec.getSize(hspec);
        setup();
    }


//    @Override
//    public boolean onTouch(View v, MotionEvent event) {
//        if (event.getAction() == MotionEvent.ACTION_DOWN) {
//            xt = event.getX();
//            yt = event.getY();
//            this.invalidate();
//        }
//        return true;
//    }
//
    @Override
    public void run() {
//        int rollTimes=k+strings.length*3-1;
//        System.out.println(Integer.toString(offset));
//        if(offset==rollTimes){
//            offset=0;
//            rollTimes=0;
//        }
//        count++;
//        System.out.println(count);
        invalidate();
        timer.postDelayed(this,3);
    }
    public void setText(String[] s){
        this.strings=s;
    }




    /////////////////////////////////////////////////////////
//    var bird,fly,gameOver,getpoint,gameBackground
    int highScore=0;
    int currentScore=0;
    int startFlag=0;
    int endFlag=0;
    int playFlag=1;
    int highScoreFlag=0;
    int birdHeight;
    float xVelocity=50;
    float ratioX,ratioY,ratio;
    Bitmap bird,background;
    float birdBitmapWidth=320f;
    float birdBitmapHeight=240f;
    int count=0;
    public void preload() {
        mResources = getResources();
        background = ((BitmapDrawable)mResources.getDrawable(R.drawable.background,null)).getBitmap();
        bird = BitmapFactory.decodeResource(getResources(), R.drawable.bird);


//        bird.setDensity(480);
//        fly = loadSound('assets/fly.mp3');
//        gameOver = loadSound('assets/Game Over.mp3');
//        getPoint = loadSound('assets/get point.mp3');
    }
    Random r=new Random();
    Pillar[] pillars=new Pillar[3];
    public void setup() {

        birdHeight=height/2;

        ratio=height/30/birdBitmapHeight;
        System.out.println(height);
        System.out.println(ratio);
//        console.log(ratioX+""+ratioY)
        birdHeightAfterRatio=height/10;
        birdWidthAfterRatio=(int)(birdBitmapWidth/birdBitmapHeight*birdHeightAfterRatio);
        pillarWidth=width/6;
        pillarGap=width/2;
        pillarHeightGap=height/3;
        for(int i=0;i<3;i++){
            pillars[i]=new Pillar(width+i*width/2,r.nextInt(height-pillarHeightGap)+pillarHeightGap);
        }
        // any additional setup code goes here
    }
    float velocity=0;
    int acceleration=100;

    public void draw() {
//        canvas.scale(ratioX,ratioY);
        //console.log(highScoreFlag)
        if(playFlag==1){
            play();
        }
        if(endFlag==1){
            endMenu();
        }
        if(startFlag==1){
            startMenu();
        }
        if(highScoreFlag==1){
            highScoreMenu();
        }

        // your "draw loop" code goes here
    }

// for info on handling key presses, see the FAQ:
// https://cs.anu.edu.au/courses/comp1720/assignments/03-simple-arcade-game/#handling-keypresses

    public void initiatePlay(){
        currentScore=0;
        birdHeight=height/2;
        velocity=0;
        int pillarHeightGap=height/4;
        for(int i=0;i<3;i++){
            pillars[i]=new Pillar(width+i*width/2,r.nextInt(height-pillarHeightGap)+pillarHeightGap);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent me) {

        float mouseX=me.getX();
        float mouseY=me.getY();
        if(playFlag==1){
            velocity=-100;
//            fly.play()
        }
        if(endFlag==1){
//            if(mouseX<width/3+width/5&&mouseX>width/3-width/5&&mouseY>height*2/3-height/8&&mouseY<height*2/3+height/8){
//                endFlag=0;
//                startFlag=1;
//            }
//            if(mouseX<width*2/3+width/5&&mouseX>width*2/3-width/5&&mouseY>height*2/3-height/8&&mouseY<height*2/3+height/8){
//                endFlag=0;
//                playFlag=1;
//                initiatePlay();
//            }
            endFlag=0;
            playFlag=1;
            initiatePlay();
        }
//        else if(startFlag==1){
//            if(mouseX<width/3+width/5&&mouseX>width/3-width/5&&mouseY>height*2/3-height/8&&mouseY<height*2/3+height/8){
//                startFlag=0;
////                console.log("!");
//                highScoreFlag=1;
//            }
//            if(mouseX<width*2/3+width/5&&mouseX>width*2/3-width/5&&mouseY>height*2/3-height/8&&mouseY<height*2/3+height/8){
//                startFlag=0;
//                playFlag=1;
//                initiatePlay();
//            }
//        }
//        else if(highScoreFlag==1){
//            if(mouseX<width/3+width/5&&mouseX>width/3-width/5&&mouseY>height*2/3-height/8&&mouseY<height*2/3+height/8){
//                highScoreFlag=0;
//                startFlag=1;
//            }
//            if(mouseX<width*2/3+width/5&&mouseX>width*2/3-width/5&&mouseY>height*2/3-height/8&&mouseY<height*2/3+height/8){
//                highScoreFlag=0;
//                playFlag=1;
//                initiatePlay();
//            }
//        }
        return true;
        // your "mouse pressed" code goes here
    }
    public void drawBird(int birdHeight){
        birdXPosition=width/3;
        Matrix matrix=new Matrix();
        matrix.postTranslate(width/3-birdBitmapWidth*3/2,birdHeight-birdBitmapHeight*3/2);
        matrix.postRotate((float)(180*Math.atan(velocity/xVelocity)/Math.PI),width/3,birdHeight);
        matrix.postScale(ratio,ratio,width/3,birdHeight);
        canvas.drawBitmap(bird,matrix,null);
        canvas.drawText(String.valueOf(birdHeight),birdXPosition,birdHeight,textPaint);
        canvas.drawText("asdf0",0,0,textPaint);
    }


    public void drawPillars(){
        for(int i=0;i<3;i++){
            // if(pillars[i]==null){
            //     console.log(pillars[(i+4)%5])
            //     //pillars[i]=new pillar(pillars[(i+4)%5].xPosition+width/5,random(height-200))
            // }
            pillars[i].xPosition-=xVelocity/30;
            if(pillars[i].xPosition<-100){
                pillars[i]=new Pillar(width*3/2-pillarWidth,r.nextInt(height-pillarHeightGap)+pillarHeightGap);
            }
            Paint pillarPaint=new Paint();
            pillarPaint.setColor(Color.rgb(0x74,0xbf,0x2e));
//            canvas.
            canvas.drawRect(pillars[i].xPosition,height,pillars[i].xPosition+pillarWidth,pillars[i].lowerHeight,pillarPaint);
            canvas.drawRect(pillars[i].xPosition,pillars[i].lowerHeight-pillarHeightGap,pillars[i].xPosition+pillarWidth,0,pillarPaint);
        }
    }


    public void highScoreMenu(){
//        push()
//        drawBackground()
//
//        rectMode(CENTER)
//
//        textAlign(CENTER,CENTER)
//        fill(255)
//        textSize(70)
//        text("High Score: "+highScore,width/2,height/3)
//        textSize(30)
//
//
//        fill(0x74,0xbf,0x2e)
//        rect(width/3,height*2/3,width/5,height/8)
//        rect(width*2/3,height*2/3,width/5,height/8)
//        fill(255)
//        text("Main Menu",width/3,height*2/3)
//        text("Play",width/3*2,height*2/3)
//        pop()
    }

    public void drawBackground(){

        canvas.drawBitmap(background,0,0,null);
    }

    public void startMenu(){

    }

    int birdXPosition;
    int birdHeightAfterRatio;
    int birdWidthAfterRatio;
    int pillarWidth;
    int pillarGap;
    int pillarHeightGap;
    Paint textPaint=new Paint();
    public void play(){
        textPaint.setColor(Color.BLACK);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setTextSize(50);

        drawBackground();
        velocity+=acceleration/30;
        birdHeight+=velocity/30;
//
        drawBird(birdHeight);

        drawPillars();
        canvas.drawText("Score: "+currentScore,width,height/5,textPaint);
        if(birdHeight<0){
            birdHeight=0;
            velocity=0;
        }
        if(birdHeight>height+120){
            endFlag=1;
            startFlag=0;
            playFlag=0;
//            gameOver.play()
        }
        for(int i=0;i<3;i++){
            if(birdXPosition+birdWidthAfterRatio/2>pillars[i].xPosition&&birdXPosition-birdWidthAfterRatio/2<pillars[i].xPosition+pillarWidth){
                if(birdHeight+birdHeightAfterRatio/2>pillars[i].lowerHeight||birdHeight-birdHeightAfterRatio/2<pillars[i].lowerHeight-pillarHeightGap){
                    System.out.println(birdHeightAfterRatio);
                    System.out.println(birdHeight);
                    System.out.println(pillars[i].lowerHeight);
                    System.out.println(pillars[i].lowerHeight-pillarGap);
                    System.out.println(birdHeight+birdHeightAfterRatio/2<pillars[i].lowerHeight);
                    System.out.println(birdHeight-birdHeightAfterRatio/2>pillars[i].lowerHeight-pillarHeightGap);
                    endFlag=1;
                    startFlag=0;
                    playFlag=0;
//                    gameOver.play()
                }
            }
            if(pillars[i].xPosition+pillarWidth<birdXPosition+birdWidthAfterRatio/2&&pillars[i].xPosition+pillarWidth+xVelocity/30>birdXPosition+birdWidthAfterRatio/2){
                currentScore+=1;
//                getPoint.play()
            }
        }
    }

    public void endMenu(){
        ma.update("end");
//        push()
//        textSize(50)
//        textAlign(CENTER,CENTER)
//        if(currentScore>highScore){
//            highScore=currentScore
//        }
//        fill(255)
//        text("Game Over",width/2,height/3)
//        textSize(30)
//        text("Score: "+currentScore+"\n"+"HighScore: "+highScore,width/2,height/2)
//        rectMode(CENTER)
//        fill(0x74,0xbf,0x2e)
//        rect(width/3,height*2/3,width/5,height/8)
//        rect(width*2/3,height*2/3,width/5,height/8)
//        fill(255)
//        text("Main Menu",width/3,height*2/3)
//        text("Restart",width/3*2,height*2/3)
//        pop()
    }
    //////////////////////////////

}