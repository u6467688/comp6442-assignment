package comp2100group.flappybird;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Handler;
import android.util.AttributeSet;

import android.util.Log;
import android.view.View;

public class Score extends View implements Runnable{
    Handler timer;
    int score = 0;
    //TODO 过一根水管加一分
    public Score(Context c, AttributeSet as)   {
        super(c, as);
        timer = new Handler();
        timer.postDelayed(this,10);
    }

    @Override
    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);
        Paint p = new Paint();
        p.setColor(Color.BLUE);
        p.setTextSize(60.0f);
        p.setTypeface(Typeface.create(Typeface.MONOSPACE, Typeface.BOLD));
        canvas.drawText("Score"+":"+score,300.0f,100.0f,p);
    }

    @Override
    protected void onMeasure(int wspec, int hspec) {
        Log.d("MyView", "w:" + MeasureSpec.toString(wspec) +
                " h:" + MeasureSpec.toString(hspec));
        if (!(MeasureSpec.getMode(wspec) == MeasureSpec.AT_MOST &&
                MeasureSpec.getMode(hspec) == MeasureSpec.AT_MOST))
            throw new AssertionError();
        setMeasuredDimension(MeasureSpec.getSize(wspec),MeasureSpec.getSize(hspec));
    }


    @Override
    public void run() {
        score++;
        invalidate();
        timer.postDelayed(this,10);
    }
}

