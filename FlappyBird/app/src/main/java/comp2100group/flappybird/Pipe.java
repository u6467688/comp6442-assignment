package comp2100group.flappybird;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import java.util.Random;

public class Pipe{
    int X_LOCATION;
    int GAP_HEIGHT;
    int GAP_WIDTH;
    int PIPE_Width;

    public int[] rect1 = new int[4];
    public int[] rect2 = new int[4];

    public Pipe(int x){
        this.X_LOCATION =x;
    }
    public void drawPipe(Canvas canvas, int width, int height){
        Paint p = new Paint();
        p.setColor(Color.rgb(0x74,0xbf,0x2e));

        PIPE_Width = width/5;
        GAP_WIDTH = height/5;
        canvas.drawRect(X_LOCATION,0,X_LOCATION+PIPE_Width,height-GAP_HEIGHT-GAP_WIDTH, p);
        canvas.drawRect(X_LOCATION,height - GAP_HEIGHT,X_LOCATION+PIPE_Width,height, p);
    }
    public void setGapHeight(int height){
        GAP_HEIGHT = height;
    }
    public Pipe generateNext(int screenWidth, int screenHeight){
        int gapCenterHeight = GAP_HEIGHT + GAP_WIDTH/2;
        int lowerBoundary = Math.min(screenHeight*4/5,gapCenterHeight + screenHeight/2);
        int upperBoundary = Math.max(screenHeight/5,gapCenterHeight - screenHeight/2);
        Random r = new Random();
        int newGapHeight = r.nextInt(lowerBoundary - upperBoundary) + upperBoundary;
        Pipe pipe = new Pipe(screenWidth);
        pipe.setGapHeight(newGapHeight + GAP_WIDTH/2);

        rect1[0] = X_LOCATION;
        rect1[1] = 0;
        rect1[2] = PIPE_Width;
        rect1[3] = screenHeight-GAP_HEIGHT-GAP_WIDTH;

        rect2[0] = X_LOCATION;
        rect2[1] = screenHeight - GAP_HEIGHT;
        rect2[2] = PIPE_Width;
        rect2[3] = screenHeight;
        return pipe;
    }

    public int getPipeWidth(){
        return PIPE_Width;
    }
}
