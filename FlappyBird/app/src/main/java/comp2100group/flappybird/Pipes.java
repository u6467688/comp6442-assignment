package comp2100group.flappybird;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

public class Pipes extends View implements Runnable{
    public ArrayList<Pipe> pipes;
    Pipe current;
    int SCREEN_WIDTH,SCREEN_HEIGHT;

    Handler timer;
    public Pipes(Context context, @Nullable AttributeSet attrs){
        super(context, attrs);
        timer = new Handler();
        timer.postDelayed(this,10);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        for(Pipe p: pipes){
            p.drawPipe(canvas, SCREEN_WIDTH, SCREEN_HEIGHT);
        }
    }

    @Override
    protected void onMeasure(int wspec, int hspec) {
        Log.d("MyView", "w:" + MeasureSpec.toString(wspec) +
                " h:" + MeasureSpec.toString(hspec));
        if (!(MeasureSpec.getMode(wspec) == MeasureSpec.AT_MOST &&
                MeasureSpec.getMode(hspec) == MeasureSpec.AT_MOST))
            throw new AssertionError();
        setMeasuredDimension(MeasureSpec.getSize(wspec), MeasureSpec.getSize(hspec));

        SCREEN_WIDTH = MeasureSpec.getSize(wspec);
        SCREEN_HEIGHT = MeasureSpec.getSize(hspec);

        //TODO
        pipes = new ArrayList<>();
        current = new Pipe(SCREEN_WIDTH);
        current.setGapHeight(SCREEN_HEIGHT/2);
        pipes.add(current);
    }

    @Override
    public void run() {
        for(Pipe p: pipes){
            p.X_LOCATION = p.X_LOCATION - SCREEN_WIDTH/200;
        }
        if(current.X_LOCATION <= SCREEN_WIDTH/3){
            Pipe tmp = current.generateNext(SCREEN_WIDTH,SCREEN_HEIGHT);
            pipes.add(tmp);
            current = tmp;
        }
        invalidate();
        timer.postDelayed(this,10);
    }
}
