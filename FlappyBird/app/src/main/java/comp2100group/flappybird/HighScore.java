package comp2100group.flappybird;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class HighScore extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_high_score);

        TextView scoreLabel=(TextView) findViewById(R.id.score);
        TextView highscoreLabel = (TextView)findViewById(R.id.highscore);
        // get score
        int score=getIntent().getIntExtra("SCORE",0);// get score
        scoreLabel.setText(score+"");
        // save and load the score
        SharedPreferences setscore =getSharedPreferences("HIGHSCORE", Context.MODE_PRIVATE);
        int highscore = setscore.getInt("HIGHSCORE",0);

        if(score>highscore){
            highscoreLabel.setText("High Score:"+score );

            // update high score
            SharedPreferences.Editor edito =setscore.edit();
            edito.putInt("HIGHSCORE",score);
            edito.commit();
        } else {
            highscoreLabel.setText("High score"+highscore);
        }

    }

    // to sttart again

//    public void
}
