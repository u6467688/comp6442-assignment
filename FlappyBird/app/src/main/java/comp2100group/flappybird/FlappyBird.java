package comp2100group.flappybird;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class FlappyBird extends View implements Runnable {
    Handler timer;
    int SCREEN_WIDTH,SCREEN_HEIGHT;
    float xVelocity;
    float velocity=0;
    int acceleration=100;

    float birdBitmapWidth=320f;
    float birdBitmapHeight=240f;

    int BIRD_X_LOCATION;
    int BIRD_Y_LOCATION;

    int hitboxHeight;

    //TODO：速度加快，开场不会自动下落
    public FlappyBird(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        timer = new Handler();
        timer.postDelayed(this,10);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Bitmap birdImage = BitmapFactory.decodeResource(getResources(), R.drawable.bird);
        Matrix matrix=new Matrix();
        float ratio=SCREEN_HEIGHT/30/birdBitmapHeight;
        matrix.postTranslate(BIRD_X_LOCATION-birdBitmapWidth*3/2,BIRD_Y_LOCATION-birdBitmapHeight*3/2);
        matrix.postRotate((float)(180*Math.atan(velocity/xVelocity)/Math.PI),BIRD_X_LOCATION,BIRD_Y_LOCATION);
        matrix.postScale(ratio,ratio,BIRD_X_LOCATION,BIRD_Y_LOCATION);
        canvas.drawBitmap(birdImage,matrix,null);

        Paint p = new Paint();
        p.setColor(Color.rgb(0x74,0xbf,0x2e));
        hitboxHeight = SCREEN_HEIGHT/10;
        canvas.drawCircle(BIRD_X_LOCATION,BIRD_Y_LOCATION,SCREEN_HEIGHT/100,p);
    }

    @Override
    protected void onMeasure(int wspec, int hspec) {
        Log.d("MyView", "w:" + MeasureSpec.toString(wspec) +
                " h:" + MeasureSpec.toString(hspec));
        if (!(MeasureSpec.getMode(wspec) == MeasureSpec.AT_MOST &&
                MeasureSpec.getMode(hspec) == MeasureSpec.AT_MOST))
            throw new AssertionError();
        setMeasuredDimension(MeasureSpec.getSize(wspec),MeasureSpec.getSize(hspec));

        SCREEN_WIDTH= MeasureSpec.getSize(wspec);
        SCREEN_HEIGHT= MeasureSpec.getSize(hspec);
        //设定鸟的初始高度
        BIRD_Y_LOCATION=SCREEN_HEIGHT/2;
        BIRD_X_LOCATION=SCREEN_WIDTH/3;
        //修改这个速率的时候请去吧pieps里run()的移动速率也改了
        xVelocity = SCREEN_WIDTH/200;
    }
    @Override
    public void run() {
        velocity+=acceleration/30;
        BIRD_Y_LOCATION+=velocity/30;

        invalidate();
        timer.postDelayed(this,3);
    }

    @Override
    public boolean onTouchEvent(MotionEvent me) {
          velocity=-100;
        return true;
    }
}