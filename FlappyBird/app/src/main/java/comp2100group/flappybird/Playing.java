package comp2100group.flappybird;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

//1.调整下落和提升速度，让他在两个水管之间能从上到下
//2.把水管的生成机制调回原来的
public class Playing extends AppCompatActivity {
    Handler handler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playing);


        handler.postDelayed(observe, 10);
    }

    private boolean Intersect(int Rect_x, int Rect_y, int Rect_width, int Rect_height, int Circle_x, int Circle_y, int Circle_radius){
        int Rect_Center_X = Rect_x + Rect_width/2;
        int Rect_Center_Y = Rect_y + Rect_height/2;

        int Vector_X = Math.abs(Rect_Center_X - Circle_x);
        int Vector_Y = Math.abs(Rect_Center_Y - Circle_y);

        Vector_X = Math.max(0,Vector_X - Rect_width/2);
        Vector_Y = Math.max(0,Vector_Y - Rect_height/2);

        double shortestDistance = Math.sqrt((double)(Vector_X * Vector_X + Vector_Y * Vector_Y));
        if(shortestDistance < Circle_radius) return true;
        else return false;
    }

    void gameover(){
        TextView time = (TextView) findViewById(R.id.time);
        time.setText(""+"true");
    }
    //负责监视bird和pipe，并且判定是否需要游戏结束
    Runnable observe = new Runnable() {
        @Override
        public void run() {
            Pipes pipes = findViewById(R.id.pipes);
            FlappyBird bird = findViewById(R.id.bird);
            if(bird.BIRD_Y_LOCATION>bird.SCREEN_HEIGHT) gameover();
            for(Pipe p: pipes.pipes){
                boolean gameover = Intersect(p.rect1[0],p.rect1[1],p.rect1[2],p.rect1[3],bird.BIRD_X_LOCATION,bird.BIRD_Y_LOCATION,bird.hitboxHeight)
                         || Intersect(p.rect2[0],p.rect2[1],p.rect2[2],p.rect2[3],bird.BIRD_X_LOCATION,bird.BIRD_Y_LOCATION,bird.hitboxHeight);
                if(gameover){
                    //跳转到结束
                    gameover();
                }
            }
            handler.postDelayed(observe, 10);
        }
    };
}
