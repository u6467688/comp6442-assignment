package comp2100group.flappybird;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/***
 * Author: Chendong Ma
 */

public class PlayingTest {
    // Test intersection.

    @Test
    public void testParallel() {
        // Test intersection when circle and the square are parallel.
        Playing p = new Playing();

        boolean b1 = p.Intersect(0, 0, 2, 2, 1, 0, 1);
        boolean b2 = p.Intersect(0, 0, 2, 2, 3, 0, 1);
        boolean b3 = p.Intersect(0, 0, 2, 2, 2, 0, 1);

        assertTrue(b1);
        assertTrue(b3);
        assertFalse(b2);
    }

    @Test
    public void testDiagonal(){
        // Test intersection when circle and square are diagonal.
        Playing p = new Playing();

        boolean b1 = p.Intersect(0, 0, 2, 2, 1, 1, 1);
        boolean b2 = p.Intersect(0, 0, 2, 2, 2, 2, 1);

        assertTrue(b1);
        assertTrue(b2);
    }
}
