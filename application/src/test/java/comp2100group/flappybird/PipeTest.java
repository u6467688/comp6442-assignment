package comp2100group.flappybird;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/***
 * Author: Chendong Ma
 */

public class PipeTest {
    // Test generateNext.

    @Test
    public void testPipe() {
        // Test if generateNext returns a required pipe.
        Pipe p = new Pipe(10, 10, 10);

        Pipe p1 = p.generateNext();

        assertEquals(10, p1.SCREEN_HEIGHT);
    }


}
