package comp2100group.flappybird;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import java.util.Random;
/***
 * Author1: Yiqing Yang
 *   - created this class based on Tiancheng's work and added the rules of the generation of pipes.
 *   - added new fields rect1 and rect2 to help the Intersect method in Playing
 * Author2: Zhe Wang & Tiancheng Wang
 *        - originally defined this class but the drawPipe method was put in another class
 */
public class Pipe{
    int LEFT_DIS;
    int RIGHT_DIS;
    int PIPE_Width;

    int BOTTOM_DIS;
    int UPPER_DIS;
    int GAP_Len;

    int SCREEN_HEIGHT;
    int SCREEN_WIDTH;


    public Pipe(int screen_height, int screen_width, int gap_height){
        LEFT_DIS = screen_width;
        PIPE_Width = screen_width/5;
        RIGHT_DIS = LEFT_DIS + PIPE_Width;

         UPPER_DIS = gap_height;
         GAP_Len = screen_height/4;
         BOTTOM_DIS = screen_height - UPPER_DIS - GAP_Len;

        SCREEN_WIDTH = screen_width;
        SCREEN_HEIGHT = screen_height;
    }
    public void drawPipe(Canvas canvas){
        Paint p = new Paint();
        p.setColor(Color.rgb(0x74,0xbf,0x2e));

        canvas.drawRect(LEFT_DIS,0,RIGHT_DIS,BOTTOM_DIS, p);
        canvas.drawRect(LEFT_DIS,SCREEN_HEIGHT - UPPER_DIS,RIGHT_DIS,SCREEN_HEIGHT, p);
    }
    public void setGapHeight(int height){
        UPPER_DIS = height;
    }

    public Pipe generateNext(){
        Random r = new Random();
        int newGapHeight = r.nextInt(SCREEN_HEIGHT*3/5) + SCREEN_HEIGHT/5;
        Pipe pipe = new Pipe(SCREEN_HEIGHT, SCREEN_WIDTH,newGapHeight);
        return pipe;
    }
}
