package comp2100group.flappybird;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import comp2100group.flappybird.FlappyBird;
import comp2100group.flappybird.HighScore;
import comp2100group.flappybird.Pipes;
import comp2100group.flappybird.R;

/***
 * Author: Yiqing Yang
 */
public class Playing extends AppCompatActivity {
    Handler handler = new Handler();

    FlappyBird bird;
    Pipes pipes;

    Intent intent;
    boolean gover;

    int score = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playing);

        handler.postDelayed(observer, 10);
        intent = new Intent(this,HighScore.class);
        pipes = findViewById(R.id.pipes);
        bird = findViewById(R.id.bird);
        gover = false;
    }

    // Check if a rectangle intersects with another circle
    boolean Intersect(int Rect_x, int Rect_y, int Rect_width, int Rect_height, int Circle_x, int Circle_y, int Circle_radius){
        int Rect_Center_X = Rect_x + Rect_width/2;
        int Rect_Center_Y = Rect_y + Rect_height/2;

        int Vector_X = Math.abs(Rect_Center_X - Circle_x);
        int Vector_Y = Math.abs(Rect_Center_Y - Circle_y);

        Vector_X = Math.max(0,Vector_X - Rect_width/2);
        Vector_Y = Math.max(0,Vector_Y - Rect_height/2);

        double shortestDistance = Math.sqrt((double)(Vector_X * Vector_X + Vector_Y * Vector_Y));
        if(shortestDistance < Circle_radius) return true;
        else return false;
    }

    // Gameover method, responsible for jumping to another activity.
    // It would be only executed once and would finish this activity while starting another activity.
    // It can avoid handler calling startActivity() method more than once, and also close the handler when we are on another activity to save memory.
    public void gameover(){
        if(!gover){
            gover = true;
            intent.putExtra("SCORE",score);
            startActivity(intent);
            finish();
        }
    }

    //It observes pipes and bird, and inspects whether the bird has crashed into a pipe or drop out of the lower boundary
    Runnable observer = new Runnable() {
        @Override
        public void run() {

            score = (Integer)pipes.pipes.size()-2;

            if(bird.BIRD_Y_LOCATION>bird.SCREEN_HEIGHT)
                gameover();
            for(Pipe p: pipes.pipes){
                boolean over = Intersect(p.LEFT_DIS,0,p.RIGHT_DIS-p.LEFT_DIS,p.BOTTOM_DIS,bird.BIRD_X_LOCATION,bird.BIRD_Y_LOCATION,bird.hitboxRadius)
                        || Intersect(p.LEFT_DIS,p.SCREEN_HEIGHT-p.UPPER_DIS,p.RIGHT_DIS-p.LEFT_DIS,p.UPPER_DIS,bird.BIRD_X_LOCATION,bird.BIRD_Y_LOCATION,bird.hitboxRadius);
                if(over)
                    gameover();
            }
            handler.postDelayed(observer, 10);
        }
    };
}
