package comp2100group.flappybird;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

/***
 * Author1: Yiqing Yang
 *      - extracted code from MyView class( Tiancheng and Zhe's work)
 *      - change the way of storing pipes
 * Author2: Zhe Wang & Tiancheng Wang
 *      - provided the idea but all code was put in an integrated MyView class
 */
public class Pipes extends View implements Runnable{
    public ArrayList<Pipe> pipes;
    Pipe current;
    int SCREEN_WIDTH,SCREEN_HEIGHT;

    Handler timer;
    public Pipes(Context context, @Nullable AttributeSet attrs){
        super(context, attrs);
        timer = new Handler();
        timer.postDelayed(this,10);

        pipes = new ArrayList<>();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        for(Pipe p: pipes){
            p.drawPipe(canvas);
        }
    }

    @Override
    protected void onMeasure(int wspec, int hspec) {
        Log.d("MyView", "w:" + MeasureSpec.toString(wspec) +
                " h:" + MeasureSpec.toString(hspec));
        if (!(MeasureSpec.getMode(wspec) == MeasureSpec.AT_MOST &&
                MeasureSpec.getMode(hspec) == MeasureSpec.AT_MOST))
            throw new AssertionError();
        setMeasuredDimension(MeasureSpec.getSize(wspec), MeasureSpec.getSize(hspec));

        SCREEN_WIDTH = MeasureSpec.getSize(wspec);
        SCREEN_HEIGHT = MeasureSpec.getSize(hspec);

        // Add the first pipe to the arraylist
        current = new Pipe(SCREEN_HEIGHT,SCREEN_WIDTH,SCREEN_HEIGHT/2);
        pipes.add(current);
    }

    @Override
    public void run() {
        //Move as the time passes
        for(Pipe p: pipes){
            p.LEFT_DIS= p.LEFT_DIS - SCREEN_WIDTH/200;
            p.RIGHT_DIS= p.RIGHT_DIS - SCREEN_WIDTH/200;
        }
        // When the last pipe has arrived the 1/3 position to the right boundary, generating the next pipe
        if(current.LEFT_DIS <= SCREEN_WIDTH/3){
            Pipe tmp = current.generateNext();
            pipes.add(tmp);
            current = tmp;
        }
        invalidate();
        timer.postDelayed(this,10);
    }
}
