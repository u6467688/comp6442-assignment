package comp2100group.flappybird;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/***
 * Author1: Yiqing Yang
 *      - extracted code from MyView class( Tiancheng and Zhe's work)
 *      - modified the hitbox and also made it visible
 * Author2: Zhe Wang & Tiancheng Wang
 *       - did the most things but all code was put in an integrated MyView class
 */

public class FlappyBird extends View implements Runnable {
    Handler timer;

    int SCREEN_WIDTH;
    int SCREEN_HEIGHT;

    float xVelocity;
    float velocity=0;
    int acceleration;

    float birdBitmapWidth=320f;
    float birdBitmapHeight=240f;

    int BIRD_X_LOCATION;
    int BIRD_Y_LOCATION;

    int hitboxRadius;

    public FlappyBird(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        timer = new Handler();
        timer.postDelayed(this,10);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // Draw the bird
        Bitmap birdImage = BitmapFactory.decodeResource(getResources(), R.drawable.bird);
        Matrix matrix=new Matrix();
        float ratio=SCREEN_HEIGHT/30/birdBitmapHeight;
        matrix.postTranslate(BIRD_X_LOCATION-birdBitmapWidth*3/2,BIRD_Y_LOCATION-birdBitmapHeight*3/2);
        matrix.postRotate((float)(180*Math.atan(velocity/xVelocity)/Math.PI),BIRD_X_LOCATION,BIRD_Y_LOCATION);
        matrix.postScale(ratio,ratio,BIRD_X_LOCATION,BIRD_Y_LOCATION);
        canvas.drawBitmap(birdImage,matrix,null);

        //Draw the hitbox
        Paint p = new Paint();
        p.setColor(Color.rgb(0x74,0xbf,0x2e));
        hitboxRadius = SCREEN_HEIGHT/50;
        canvas.drawCircle(BIRD_X_LOCATION,BIRD_Y_LOCATION,SCREEN_HEIGHT/50,p);
    }

    @Override
    protected void onMeasure(int wspec, int hspec) {
        Log.d("MyView", "w:" + MeasureSpec.toString(wspec) +
                " h:" + MeasureSpec.toString(hspec));
        if (!(MeasureSpec.getMode(wspec) == MeasureSpec.AT_MOST &&
                MeasureSpec.getMode(hspec) == MeasureSpec.AT_MOST))
            throw new AssertionError();
        setMeasuredDimension(MeasureSpec.getSize(wspec),MeasureSpec.getSize(hspec));

        SCREEN_WIDTH= MeasureSpec.getSize(wspec);
        SCREEN_HEIGHT= MeasureSpec.getSize(hspec);
        //Initialize the height of the bird
        BIRD_Y_LOCATION=SCREEN_HEIGHT/2;
        BIRD_X_LOCATION=SCREEN_WIDTH/3;

        xVelocity = SCREEN_WIDTH/200;
        acceleration = SCREEN_HEIGHT/8;
    }
    @Override
    public void run() {
        velocity+=acceleration/30;
        BIRD_Y_LOCATION+=velocity/30;

        invalidate();
        timer.postDelayed(this,10);
    }

    @Override
    public boolean onTouchEvent(MotionEvent me) {
        velocity=- SCREEN_HEIGHT/8;
        return true;
    }
}