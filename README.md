# Retro Game - _Flappy Bird_

## Team structure and roles 
+ TianCheng Wang u6467688 - team leader, time handler, game process (start, end, run)
+ Zhe Wang u6308175 - team task allocation and management, game screen adjustment and layout,scores justification
+ Chendong Ma u6011771 - note taker, welcome and end activities, git mamagement, report writting, picture material
+ Yiwen Peng u6071714 - score calculation, high score persistence, score activity
+ Yiqing Yang u6202004 - modified code structure(divided the integrated MyView into servearl parts, cleaned up unused code, combined everyone's work and did debug things)

## Game Overview 

![game_screen_shot](/uploads/f8f947bddb4876d058fabf8fc9997acf/FlappyBird.png)

The game was built on Android Studio with view and movement codings. The red bird, if screen not touched, would drop from an initial point. Every time the screen was touched, the bird would bounce up at a fixed velocity. There are sets of pipes moving on the screen, the bird would need to avoid touching the pipes by keeping bouncing up. Once touched, the bird would be killed and game over. The score adds up as time goes by, when the bird was killed, the score would be its score for this round of game. 


## Design Documentation 
+ [Design Summary]
    - The design includes three activities: Welcome activity, Playing activity, HighScore activity.
       - Welcome page is simply a start page, containing the title and a button linked to Playing activity.
       - Playing activity holds the main body of the whole game, and details will be given below.
       - HighScore activiy shows the score and also records the history best score. It allows players to go back to the title page or replay the game.
    - In the Playing activity, there was initially one MyView.java, before it was broken into several classes. 
        - The FlappyBird class is the bird view class. The bird was drawn on the cavas in this class, and was set movement with a velocity, which depend on a fixed acceleration. When screen was touched, the bird bounce up by changing its velocity to negative value.
        - The Pipe class set one set of "pipe" in the game, it was drawn in this class. And there is a generateNext method to be used in Pipes class to generate another set of pipes, when this set of pipes run away from the screen.
        - The Pipes class is the pipe view class. It makes the "pipes" movable in some fixed speed. 
        - The score class was to calculate and show the current scores. 
        - The Playing activity class contains an Intersect method, to justify intersection of birds and pipes. And the observe method is to observe the bird and pipes, to determine if reached a game over (intersection). 

+ [UML Diagram]
    ![UML](/uploads/1b947fb14f054a2663b0c364e5279a0b/UML.png)

+ [Testing Summary]
    The java code methods Intersection and generateNext were tested by writing Android JUnit tests. The onDraw, onMeasure methods and xml fils are hard to test using Android tests, so their testing was just conducted by running the game.

## Minuted Meetings
+ [Meeting 1 - 22ed March - team formation and game decision] 
    - Find and decide team members
    - Decide team structure
        - Tiancheng Wang became the team leader
    - Game discussion and decision
    - Left each member to review konwledge from Android Development, and to come up with at least basic ideas before next meeting.
+ [Meeting 2 - 20th April - divide up tasks]
    - The team decided to accept the idea from Tiancheng Wang, and to build the game based on the structure he promoted.
    - An initial divide up of tasks. 
    - TianCheng Wang provided with project skeleton
    - Zhe Wang would be in charge of screen adjustment and background music. 
    - Chendong Ma and Yiwen Peng in charge of welcome, end activities and score related classes
      
+ [Meeting 3 - 5th May - questions discussion, reassignment of tasks]
    - Based on the task completement, discuss which part is completed and which still needs modification. 
    - Reassignment of tasks based on the promoted modifications.
    - Use gitlab issued to record the parts required to be modified.
+ [Meeting 4 - 17th May - review and practice demo]
    - Try to run the game. Found some issues like Mac not workable or AVD not suit. Managed to fix them. 
    - The game runs really slowly but worked well

## Statement of Originality

I _Tiancheng Wang_ declare that everything I have submitted in this
assignment is entirely my own work, with exceptions given below.

I _Yiqing Yang_ declare that everything I have submitted in this
assignment is entirely my own work, with exceptions given below.

I _Zhe Wang_ declare that everything I have submitted in this
assignment is entirely my own work, with exceptions given below.

I _Chendong Ma_ declare that everything I have submitted in this
assignment is entirely my own work, with exceptions given below.

I _Yiwen Peng_ declare that everything I have submitted in this
assignment is entirely my own work, with exceptions given below.
the "SharedPreferences" from https://stackoverflow.com/questions/8407286/need-to-save-a-high-score-for-an-android-game
### Inspiration

The game idea was inspired by the famous game Flappy Bird.

### Code


### Assets 

The pictures are from [Fandom Wiki](http://flappybird.wikia.com/wiki/File:Mobile_-_Flappy_Bird_-_Version_12_Sprites.png), with some modifications.
